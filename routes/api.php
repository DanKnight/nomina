<?php

use Illuminate\Http\Request;
//use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/empleado', 'EmpleadosController@crearEmpleado');
Route::get('/empleados', 'EmpleadosController@listadoEmpleados');
Route::put('/empleado', 'EmpleadosController@editarEmpleado');
Route::put('/estado', 'EmpleadosController@editarEstadoEmpleado');
Route::delete('/empleado', 'EmpleadosController@eliminarEmpleados');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
