import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Dashboard from "./Dashboard";

export default class Example extends Component {
    render() {
        return (
            <div>
                <div className="row justify-content-center">
                    <div className="col-m-12">
                        <div className="card">
                            <Dashboard/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

if (document.getElementById('example')) {
    ReactDOM.render(<Example />, document.getElementById('example'));
}
