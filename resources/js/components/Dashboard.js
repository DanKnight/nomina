import React, {Component} from 'react';
import {Button, Table} from "react-bootstrap";
import axios from 'axios';
import {map} from "react-bootstrap/ElementChildren";

export default class Dashboard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            empleados: ""
        };
        // ejecutar funciones al entrar a la pagina
        this.obtenerEmpleados();
    }

    obtenerEmpleados = () => {
        axios.get('http://127.0.0.1:8000/api/empleados').then((response) => {
            // Enviar informacion al state
            this.setState({
                empleados: response.data.data
            });
        }).catch(error => {
        });
    }

    render() {
        let empleados = this.state.empleados;
        return (
            <div>
                <div className="justify-content-center">
                    <div>
                        <Button variant={"secondary"}> Agregar Empleado</Button>
                        <Table striped bordered hover>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Codigo</th>
                                <th>Nombre</th>
                                <th>Apellido Paterno</th>
                                <th>Apellido Materno</th>
                                <th>Correo</th>
                                <th>Tipo de Contrato</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            {
                                empleados.length != 0 ?
                                    <tbody>
                                    {
                                        empleados.map(empleado =>
                                            <tr>
                                                <td>{empleado.id}</td>
                                                <td>{empleado.codigo}</td>
                                                <td>{empleado.nombre}</td>
                                                <td>{empleado.apellidoPaterno}</td>
                                                <td>{empleado.apellidoMaterno}</td>
                                                <td>{empleado.correoElectronico}</td>
                                                <td>{empleado.tipoContrato}</td>
                                                <td>{empleado.estado}</td>
                                                <td>
                                                    <Button variant={"info"}>Editar</Button>
                                                    <Button variant={"danger"}>Eliminar</Button>
                                                </td>
                                            </tr>
                                        )
                                    }
                                    </tbody>
                                    :
                                    <span>No hay datos</span>
                            }
                        </Table>
                    </div>
                </div>
            </div>
        );
    }
}
