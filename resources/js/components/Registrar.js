import React, {Component} from 'react';
import {Link} from 'react-router-dom'
import {Api, login, redireccionWeb} from '../utils';
import {Button, Form, FormGroup, Alert} from "react-bootstrap";
import axios from 'axios';

export default class Registrar extends Component {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    // Funcion para agregar los datos de formulario al state
    cargardatos = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        let data = {};
        data[name] = value;
        this.setState(data);
    };

    render() {
        const mensaje = this.state.mensaje;

        return (
            <div>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-8">
                            <div className="card">
                                <div className="card-header text-center">Registrar Empleado</div>

                                <div className="card-body">
                                    <Form>
                                        <FormGroup>
                                            <Form.Label htmlFor="nombre">Nombre</Form.Label>
                                            <Form.Control type="text" name="nombre" onChange={this.cargardatos}/>
                                        </FormGroup>
                                        <FormGroup>
                                            <Form.Label htmlFor="grupo">Grupo</Form.Label>
                                            <Form.Control as="select" name="grupo"  onChange={this.cargardatos}>
                                                {
                                                    grupos != null ?
                                                        grupos.map((grupo, key) =>
                                                            <option key={key} value={grupo.id}>{grupo.nombreGrupo}</option>
                                                        )
                                                        :
                                                        <option>No hay opciones</option>
                                                }
                                            </Form.Control>
                                        </FormGroup>
                                        <Button className="text-center mb-4 mr-1" color="success" onClick={this.registrar}>
                                            Registrar
                                        </Button>
                                        <Button className="text-center mb-4" variant="secondary" onClick={this.cancelar}>
                                            Cancelar
                                        </Button>
                                    </Form>
                                    {
                                        mensaje.length != 0 ?
                                            <Alert variant="danger" onClose={() => this.setState({mensaje: ""})} dismissible>
                                                {mensaje}
                                            </Alert>
                                            :
                                            null
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
