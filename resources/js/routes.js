import React from 'react';
import {Switch, BrowserRouter, Route} from "react-router-dom";
import Home from '../components/Home';
import Dashboard from "../components/Dashboard";

import Registrar from "./components/Registrar";
function Router(props) {
    return (
        <BrowserRouter>
            <Switch>
                <Route component={Dashboard} path="/dashboard" exact/>
                <Route component={Registrar} path="/registrar" exact/>
            </Switch>
        </BrowserRouter>
    );
}
export default Router;
