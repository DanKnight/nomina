<?php

namespace App\Http\Controllers;

use App\Empleados;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmpleadosController extends Controller
{
    // Declaracion de status para las peticiones
    private $success = 200;
    private $error = 404;

    // Declaracion de mensajes para respuesta de las peticiones
    private $mensajeError = "No se puedo realizar, verificar información";
    private $mensajeSuccess = "Se realizó correctamente";

    public function listadoEmpleados() {
        $empleados = Empleados::all();

        return response()->json(['data' => $empleados, 'mensaje' => $this->mensajeSuccess, 'status' => $this->success]);
    }

    public function CrearEmpleado(Request $request) {
        // Validar los campos del request
        $validar = Validator::make($request->all(), [
            'codigo' => 'required|integer',
            'nombre' => 'required|string',
            'apellidoP' => 'required|string',
            'apellidoM' => 'required|string',
            'correo' => 'required|email',
            'tipoContrato' => 'required'
        ]);

        if ($validar->fails()) {
            return response($validar->errors()->toJson(), $this->error);
        }

        // Crear empleado
        $empleado = new Empleados;
        $empleado->codigo = $request->get('codigo');
        $empleado->nombre = $request->get('nombre');
        $empleado->apellidoPaterno = $request->get('apellidoP');
        $empleado->apellidoMaterno = $request->get('apellidoM');
        $empleado->tipoContrato = $request->get('tipoContrato');
        $empleado->correoElectronico = $request->get('correo');
        $empleado->estado = "A";
        $empleado->save();

        // retornar respuesta json
        return response()->json(['data' => $empleado, 'mensaje' => $this->mensajeSuccess, 'status' => $this->success]);
    }

    public function editarEmpleado (Request $request) {
        // Validar los campos del request
        $validar = Validator::make($request->all(), [
            'nombre' => 'required|string',
            'apellidoP' => 'required|string',
            'apellidoM' => 'required|string',
            'correo' => 'required|email',
            'tipoContrato' => 'required'
        ]);

        if ($validar->fails()) {
            return response($validar->errors()->toJson(), $this->error);
        }

        // buscar empleado en la base de datos
        $empleado = Empleados::find($request->get('id'));
        if (!$empleado) {
            // retornar respuesta json
            return response()->json(['data' => $empleado, 'mensaje' => $this->mensajeError, 'status' => $this->error]);
        }
        // editar nuevos datos
        $empleado->nombre = $request->get('nombre');
        $empleado->apellidoPaterno = $request->get('apellidoP');
        $empleado->apellidoMaterno = $request->get('apellidoM');
        $empleado->tipoContrato = $request->get('tipoContrato');
        $empleado->correoElectronico = $request->get('correo');
        $empleado->save();

        // retornar respuesta json
        return response()->json(['data' => $empleado, 'mensaje' => $this->mensajeSuccess, 'status' => $this->success]);
    }

    public function eliminarEmpleados (Request $request) {
        // buscar empleado en la base de datos
        $empleado = Empleados::find($request->get('id'));

        if (!$empleado) {
            // retornar respuesta json
            return response()->json(['data' => $empleado, 'mensaje' => $this->mensajeError, 'status' => $this->error]);
        }

        // editar nuevos datos
        $empleado->estado = $request->get('estado');
        $empleado->save();
        $empleado->delete();

        // retornar respuesta json
        return response()->json(['data' => $empleado, 'mensaje' => $this->mensajeSuccess, 'status' => $this->success]);
    }

    public function editarEstadoEmpleado (Request $request) {
        // Validar los campos del request
        $validar = Validator::make($request->all(), [
            'estado' => 'required'
        ]);

        if ($validar->fails()) {
            return response($validar->errors()->toJson(), $this->error);
        }

        // buscar empleado en la base de datos
        $empleado = Empleados::find($request->get('id'));

        if (!$empleado) {
            // retornar respuesta json
            return response()->json(['data' => $empleado, 'mensaje' => $this->mensajeError, 'status' => $this->error]);
        }
        // editar nuevos datos
        $empleado->estado = $request->get('estado');
        $empleado->save();

        // retornar respuesta json
        return response()->json(['data' => $empleado, 'mensaje' => $this->mensajeSuccess, 'status' => $this->success]);
    }
}
