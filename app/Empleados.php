<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Empleados extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id', 'codigo', 'nombre', 'apellidoPaterno', 'apellidoMaterno', 'correoElectronico', 'tipoContrato', 'estado'
    ];
}
